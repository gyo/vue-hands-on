module.exports = {
  base: "/vue-hands-on/",
  dest: "public",
  title: "おためし Vue.js",
  description: "ちょっと触ってみたい人のためのハンズオン",
  themeConfig: {
    nav: [{ text: "GitLab", link: "https://gitlab.com/gyo/vue-hands-on/" }],
    sidebar: {
      "/": ["/", "introduction", "overview", "documents", "how-to-write-1", "how-to-write-2", "build"]
    }
  }
};
