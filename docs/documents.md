# Vue の調べ方

## 公式ドキュメント

[Vue.js](https://jp.vuejs.org/index.html)

コントリビューターのおかげで、ほとんどの部分が日本語に翻訳されています。だいたいの悩みはここを読めば解決します。

## 入門書

[基礎から学ぶ Vue.js | mio |本 | 通販 | Amazon](https://www.amazon.co.jp/dp/4863542453)

最近出版された、とても評判のいい Vue.js の入門書です。

[Vue.js 入門 基礎から実践アプリケーション開発まで | 川口 和也, 手島 拓也, 野田 陽平, 喜多 啓介, 片山 真也 |本 | 通販 | Amazon](https://www.amazon.co.jp/dp/4297100916)

2018 年 9 月出版予定の、Vue.js の入門書です。Vue.js のコントリビューターによる著書です。

## Vue.js ではじめる Web 制作

[gyo / start-with-vue · GitLab](https://gitlab.com/gyo/start-with-vue)

HTML も分からない、という人向け。2018 年 8 月現在、制作途中です。（宣伝）
