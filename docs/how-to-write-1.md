# Vue の書き方 その 1

実際に Vue を書くには、そのための開発環境を構築する必要があります。

ただ、今回のハンズオンは Vue 本体に慣れることが目的なので、自分で環境を構築しなくても Vue を書けるようなサービスを利用することにします。

CodeSandbox を使って、実際に書いてみましょう。

[CodeSandbox: Online Code Editor Tailored for Web Application Development](https://codesandbox.io/)

1.  Create Sandbox ボタンをクリック
1.  New Sandbox の一覧から、Vue を選択
1.  Vue Template が開くので、Ctrl + S でプロジェクトを複製
1.  この時点で、すでに public な Web ページが作成されています

App.vue を開き、中を書き換えていきましょう。

## 書く場所

HTML は `<template>` ブロックに、JavaScript は `<script>` ブロックに、CSS は `<style>` ブロックに書きます。

## v-bind

JS の値を HTML に表示するには、`v-bind` や Mustache 構文 `{{}}` を使います。

単に文字を表示したいときは Mustache 構文、HTML 要素に属性を追加したいときは `v-bind` を使うことになります。

また `v-bind` は、省略形の `:` をよく使います。

### サンプル

template

```vue
<template>
  <div>
    <div>
      <a v-bind:href="googleUrl">{{ googleLinkText }}</a>
    </div>

    <div>
      <a :href="amazonUrl">{{ amazonLinkText }}</a>
    </div>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      googleUrl: "https://www.google.com/",
      googleLinkText: "Google",
      amazonUrl: "https://www.amazon.co.jp/",
      amazonLinkText: "Amazon"
    };
  }
};
</script>
```

### 表示結果

出力される HTML はこのようになります。

`href` 属性に URL が反映されていることと、`<a>` 要素の中のテキストが反映されていることがわかります。

```html
<div>
  <div>
    <a href="https://www.google.com/">Google</a>
  </div>

  <div>
    <a href="https://www.amazon.co.jp/">Amazon</a>
  </div>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/v-bind.png')" alt="" width="375" style="border: 1px solid #eaecef">

## v-if

条件分岐には、`v-if` を使います。

なんらかの条件に応じて要素を出し分けたいときに、要素の表示非表示を判定するために使います。

### サンプル 1

template

```vue
<template>
  <div>
    <div v-if="season === '春'">サクラ</div>
    <div v-if="season === '夏'">ヒマワリ</div>
    <div v-if="season === '秋'">コスモス</div>
    <div v-if="season === '冬'">ツバキ</div>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      season: "春"
    };
  }
};
</script>
```

### 表示結果 1

出力される HTML はこのようになります。

`<script>` ブロックで `season: "春"` と指定したため、`v-if="season === '春'"` の条件が満たされ、"サクラ" が表示されます。

`season === '夏'`, `season === '秋'`, `season === '冬'` は条件が満たされないので、表示されません。

```html
<div>
  <div>サクラ</div>
  <!---->
  <!---->
  <!---->
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/v-if-1.png')" alt="" width="375" style="border: 1px solid #eaecef">

### サンプル 2

`season: "夏"` と指定する場合は、次のようになります。

template

```vue
<template>
  <div>
    <div v-if="season === '春'">サクラ</div>
    <div v-if="season === '夏'">ヒマワリ</div>
    <div v-if="season === '秋'">コスモス</div>
    <div v-if="season === '冬'">ツバキ</div>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      season: "夏"
    };
  }
};
</script>
```

### 表示結果 2

出力される HTML はこのようになります。

```html
<div>
  <!---->
  <div>ヒマワリ</div>
  <!---->
  <!---->
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/v-if-2.png')" alt="" width="375" style="border: 1px solid #eaecef">

## v-for

配列の要素を繰り返し表示したいときには、反復処理のための `v-for` を使います。

### サンプル

template

```vue
<template>
  <div>
    <div v-for="item in items">{{ item }}</div>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      items: ["りんご", "みかん", "さくらんぼ"]
    };
  }
};
</script>
```

### 表示結果

出力される HTML はこのようになります。

`<script>` ブロックで定義した配列 `items` の要素が順番に表示されています。

```html
<div>
  <div>りんご</div>
  <div>みかん</div>
  <div>さくらんぼ</div>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/v-for.png')" alt="" width="375" style="border: 1px solid #eaecef">

## v-on

ボタンをクリックしたときなどに実行する処理を登録するには、`v-on` を使います。

省略形の `@` をよく使います。

### サンプル

template

```vue
<template>
  <div>
    <button @click="myAlert()">CLICK ME</button>
  </div>
</template>
```

script

```vue
<script>
export default {
  methods: {
    myAlert() {
      alert("Vue.js 入門！");
    }
  }
};
</script>
```

### 表示結果

画面上の見た目はこのようになります。

<img :src="$withBase('/v-on-1.png')" alt="" width="375" style="border: 1px solid #eaecef">

ボタンをクリックすると、アラートウィンドウが表示されます。

<img :src="$withBase('/v-on-2.png')" alt="" width="375" style="border: 1px solid #eaecef">

## Scoped CSS

`<style>` ブロックに `scoped` 属性を設定すると、グローバルに影響を与えない形で CSS を書くことができます。

### サンプル

template

```vue
<template>
  <div>
    <p>App.vue に書いた p 要素です。</p>
  </div>
</template>
```

style

```vue
<style scoped>
p {
  color: green;
}
</style>
```

グローバルへの影響がないことを確認するため、index.html に `<p>` 要素を書いてみましょう。

index.html

```html
<body>
  <p>index.html に書いた p 要素です。</p>
  <div id="app"></div>
  <!-- built files will be auto injected -->
</body>
```

### 表示結果

画面上の見た目はこのようになります。

index.html に書いた `<p>` 要素の見た目は変わらす、App.vue に書いた `<p>` 要素の見た目だけが変化しているのが確認できます。

<img :src="$withBase('/scoped-css.png')" alt="" width="375" style="border: 1px solid #eaecef">

## import

作ったコンポーネントを他のコンポーネントで使いたいときには、`import` 構文を使います。

### サンプル

components ディレクトリに、ChildComponent.vue というファイルを作り、App.vue で読み込んでみましょう。

まずは、`<template>` ブロックだけを持つ ChildComponent.vue を作ってください。

components/ChildComponent.vue - template

```vue
<template>
  <div>
    <div>わたしは ChildComponent です。</div>
  </div>
</template>
```

次に、App.vue で `import` 構文を使って ChildComponent.vue を読み込み、読み込んだコンポーネントを `components: {}` に登録してください。

登録した名前のタグが `<template>` ブロックで使えるようになります。

App.vue - script

```vue
<script>
import ChildComponent from "./components/ChildComponent.vue";

export default {
  components: {
    ChildComponent
  }
};
</script>
```

App.vue - template

```vue
<template>
  <div>
    <ChildComponent></ChildComponent>
  </div>
</template>
```

### 表示結果

出力される HTML はこのようになります。

```html
<div>
  <div>
    <div>わたしは ChildComponent です。</div>
  </div>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/import.png')" alt="" width="375" style="border: 1px solid #eaecef">
