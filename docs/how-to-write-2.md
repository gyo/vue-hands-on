# Vue の書き方 その 2

Vue には、UI 構築に便利な API が用意されています。

## data

これまでにも登場してきましたが、そのコンポーネントが持つ値です。

`data` の値はコンポーネントの内部から変更できます。

### サンプル

template

```vue
<template>
  <div>
    <div>年齢：{{ age }}</div>
    <button @click="beOld()">BE OLD</button>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      age: 15
    };
  },
  methods: {
    beOld() {
      this.age++;
    }
  }
};
</script>
```

### 表示結果

出力される HTML はこのようになります。

```html
<div>
  <div>年齢：15</div>
  <button>BE OLD</button>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/data.png')" alt="" width="375" style="border: 1px solid #eaecef">

ボタンを押すたびに、年齢が増えていきます。

## props

import したコンポーネントから import されたコンポーネントに値や関数を渡したいときに使います。

App.vue - template

```vue
<template>
  <div>
    <ChildComponent name="太郎"></ChildComponent>
  </div>
</template>
```

App.vue - script

```vue
<script>
import ChildComponent from "./components/ChildComponent.vue";

export default {
  components: {
    ChildComponent
  }
};
</script>
```

components/ChildComponent.vue - template

```vue
<template>
  <div>
    <div>名前：{{ name }}</div>
  </div>
</template>
```

components/ChildComponent.vue - script

```vue
<script>
export default {
  props: ["name"]
};
</script>
```

### 表示結果

出力される HTML はこのようになります。

```html
<div>
  <div>
    <div>名前：太郎</div>
  </div>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/props.png')" alt="" width="375" style="border: 1px solid #eaecef">

## computed

`data` や `props` をもとになんらかの計算をした結果が欲しいときに使います。

たとえば、年齢のデータをもとに成人しているかどうかを判定するような場合がこれにあたります。

### サンプル

template

```vue
<template>
  <div>
    <div>年齢：{{ age }}</div>
    <button @click="beOld()">BE OLD</button>
    <div v-if="isAdult">大人です。</div>
  </div>
</template>
```

script

```vue
<script>
export default {
  data() {
    return {
      age: 15
    };
  },
  computed: {
    isAdult() {
      return this.age > 19;
    }
  },
  methods: {
    beOld() {
      this.age++;
    }
  }
};
</script>
```

### 表示結果

出力される HTML はこのようになります。

20 歳未満の場合はこのようになります。

```html
<div>
  <div>年齢：15</div>
  <button>BE OLD</button>
  <!---->
</div>
```

20 歳以上になると、「大人です。」というメッセージが表示されます。

```html
<div>
  <div>年齢：20</div>
  <button>BE OLD</button>
  <div>大人です。</div>
</div>
```

画面上の見た目はこのようになります。

<img :src="$withBase('/computed-1.png')" alt="" width="375" style="border: 1px solid #eaecef">

20 歳以上の場合はこのようになります。

<img :src="$withBase('/computed-2.png')" alt="" width="375" style="border: 1px solid #eaecef">
