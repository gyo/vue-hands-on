const VueLoaderPlugin = require("vue-loader/lib/plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = env => {
  return {
    mode: env.production ? "production" : "development",
    entry: {
      app: "./sample/src/js/app.js"
    },
    output: {
      filename: "[name].js",
      path: __dirname + "/sample/dist/js"
    },
    plugins: [new VueLoaderPlugin(), env.production ? new BundleAnalyzerPlugin() : false].filter(plugin => plugin),
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: "vue-loader"
        },
        {
          test: /\.js$/,
          loader: "babel-loader"
        },
        {
          test: /\.css$/,
          use: ["vue-style-loader", "css-loader"]
        }
      ]
    },
    resolve: {
      alias: {
        vue$: "vue/dist/vue.esm.js"
      }
    }
  };
};
